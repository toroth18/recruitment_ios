//
//  Attributes.swift
//  Recruitment-iOS
//
//  Created by Blazej Krupa on 8/10/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol Attributes {
    var name: String { get set }
    var color: String { get set }
}
