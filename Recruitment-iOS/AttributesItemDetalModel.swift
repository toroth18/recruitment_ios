//
//  AttributesItemDetalModel.swift
//  Recruitment-iOS
//
//  Created by Blazej Krupa on 8/10/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

class AttributesItemDetalModel: Attributes, Codable {
    var name: String
    
    var color: String
    
    var desc: String = ""
    
    init(name: String, color: String, desc: String) {
        self.name = name
        self.color = color
        self.desc = desc
    }
}
