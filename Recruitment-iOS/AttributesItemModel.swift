//
//  AttributesItemModel.swift
//  Recruitment-iOS
//
//  Created by Blazej Krupa on 8/10/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

class AttributesItemModel: Attributes, Codable {
    var name: String
    
    var color: String
    
    var preview: String = ""
    
    init(name:String, color: String, preview: String) {
        self.name = name
        self.color = color
        self.preview = preview
    }
}
