//
//  DetailsViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController, NetworkingManagerDelegate {
    
    @IBOutlet weak var textView: UITextView!
    
    var itemViewModel: ItemViewModel? {
        didSet {
            if let unwrapItem = itemViewModel {
                self.view.backgroundColor = unwrapItem.color
                self.title = unwrapItem.name.customTitle(with: 2)
                NetworkingManager.sharedManager.delegate = self
                NetworkingManager.sharedManager.downloadItems(type: ItemDetailDataModel.self, url: EndPoint.base.rawValue + unwrapItem.id.json)
            }
        }
    }
    
    var itemDetailViewModel: ItemDetailViewModel? {
        didSet {
            if let unwrapItem = itemDetailViewModel {
                self.textView.text = unwrapItem.desc
            }
        }
    }
    
    deinit {
        print("Deinit",self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadItemDetail()
    }
    
    func loadItemDetail() {
        if  let id = itemViewModel?.id,
            let itemDetail = SessionManager.Instance.itemDetails.first(where: { $0.data.id == id }) {
            self.itemDetailViewModel = ItemDetailViewModel(model: itemDetail)
        }
    }
    
    func downloadedItems(result: Result<Any, NetworkError>) {
        switch result {
        case .success(let object):
            if let obj = object as? ItemDetailDataModel {
                SessionManager.Instance.itemDetails.insert(obj)
                if obj.data.id == itemViewModel?.id {
                    itemDetailViewModel = ItemDetailViewModel(model: obj)
                }
            }
        case .failure(let error):
            print(error.localizedDescription)
        }
    }
}
