//
//  Item.swift
//  Recruitment-iOS
//
//  Created by Blazej Krupa on 8/10/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol Item {
    associatedtype T: Attributes
    var id: String { get set }
    var type: String { get set }
    var attributes: T { get set }
}
