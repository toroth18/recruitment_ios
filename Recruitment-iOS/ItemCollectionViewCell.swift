//
//  ItemCollectionViewCell.swift
//  Recruitment-iOS
//
//  Created by Blazej Krupa on 8/11/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var label: UILabel!
}
