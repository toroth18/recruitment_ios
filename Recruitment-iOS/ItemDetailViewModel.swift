//
//  ItemDetailViewModel.swift
//  Recruitment-iOS
//
//  Created by Blazej Krupa on 8/11/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

class ItemDetailViewModel {
    var desc: String
    
    init(model: ItemDetailDataModel) {
        self.desc = model.data.attributes.desc
    }
}
