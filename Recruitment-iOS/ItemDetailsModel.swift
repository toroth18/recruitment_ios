//
//  ItemDetailsModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class ItemDetailDataModel: Codable, Hashable {
    static func == (lhs: ItemDetailDataModel, rhs: ItemDetailDataModel) -> Bool {
        return lhs.data == rhs.data
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(data)
    }
    
    var data: ItemDetailModel
}

class ItemDetailModel: Item, Codable, Hashable {
    
    static func == (lhs: ItemDetailModel, rhs: ItemDetailModel) -> Bool {
        return lhs.id == rhs.id && lhs.type == rhs.type
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(type)
    }
    
    var id: String
    
    var type: String
    
    var attributes: AttributesItemDetalModel
    
    typealias T = AttributesItemDetalModel
    
    init(id: String, type: String, attr: T) {
        self.id = id
        self.type = type
        self.attributes = attr
    }
}
