//
//  ItemViewModel.swift
//  Recruitment-iOS
//
//  Created by Blazej Krupa on 8/11/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemViewModel {
    let id: String
    let name: String
    let preview: String
    let color: UIColor
    
    init(model: ItemModel) {
        self.id = model.id
        self.name = model.attributes.name
        self.color = model.attributes.color.colorValue
        self.preview = model.attributes.preview
    }
}
