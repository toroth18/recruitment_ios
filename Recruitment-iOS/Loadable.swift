//
//  Loadable.swift
//  Recruitment-iOS
//
//  Created by Blazej Krupa on 8/11/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol Loadable {
    func loadData()
    func getItems() -> [ItemViewModel]
}

extension Loadable {
    func getItems() -> [ItemViewModel] {
        return SessionManager.Instance.items
            .map { ItemViewModel(model: $0) }
            .sorted(by: { $0.id < $1.id })
    }
}

extension Loadable where Self : CollectionViewController {
    func loadData() {
        self.itemViewModels = getItems()
    }
}

extension Loadable where Self : TableViewController {
    func loadData() {
        self.itemViewModels = getItems()
    }
}
