//
//  NetworkingManager.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

enum NetworkError: Error {
    case requestFail
    case decodeFail
    case connectionFail
    case unknow
}

enum EndPoint: String {
    case base = "Item"
}

protocol Requestable {
    func downloadItems<T: Codable>(type: T.Type, url: String)
    func request(filename:String, completionBlock:@escaping (Data?) -> Void)
}

protocol NetworkingManagerDelegate: class {
    /// Delegate method when data was downloaded
    ///
    /// - Parameter result: Result value where **Any** ( first parameter ) represent data which is downloaded. When we are downloading only one value it should looks like :
    /// switch result {
    ///    case .success(let object):
    ///    if let obj = object as? **Any of Model **  {
    ///    itemViewModels = obj.data.map { ItemViewModel(model: $0) }
    ///    tableView.reloadData()
    ///    }
    ///    case .failure(let error):
    ///    print(error.localizedDescription)
    /// }
    
    /// When we are doing more then one request in view controller we should change "if" inside of "Success" case to normal switch eg.:
    /// switch object {
    ///    case is Model1:
    ///    case is Model2:
    ///    default:
    /// }
    func downloadedItems(result: Result<Any,NetworkError>)
}

class NetworkingManager: NSObject, Requestable {
    
    static var sharedManager = NetworkingManager()
    
    weak var delegate:NetworkingManagerDelegate?
    
    func downloadItems<T>(type: T.Type, url: String) where T : Decodable, T : Encodable {
        request(filename: url) { (data) in
            if  let unwrapData = data,
                let items = try? JSONDecoder().decode(T.self, from: unwrapData) {
                self.delegate?.downloadedItems(result: .success(items))
            } else {
                self.delegate?.downloadedItems(result: .failure(.decodeFail))
            }
        }
    }
    
    internal func request(filename:String, completionBlock:@escaping (Data?) -> Void) {
        // if request fail, we can add this case here
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            let data = JSONParser.jsonDataFromFilename(filename)
            completionBlock(data)
        }
    }
    
}
