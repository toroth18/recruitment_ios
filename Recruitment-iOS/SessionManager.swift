//
//  SessionManager.swift
//  Recruitment-iOS
//
//  Created by Blazej Krupa on 8/11/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

// at this MVP is better to use session manager to reduce slow connection issue, but if application start grwoing it better to add databse
class SessionManager {
    static var Instance: SessionManager = SessionManager()
    var items: Set<ItemModel> = []
    var itemDetails: Set<ItemDetailDataModel> = []
}
