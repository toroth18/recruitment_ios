//
//  StringEx.swift
//  Recruitment-iOS
//
//  Created by Blazej Krupa on 8/9/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

extension String {
    func customTitle(with dividend: Int) -> String {
        var updatedTitle = ""
        
        for (index,string) in self.enumerated() {
            if index % dividend == 0 {
                updatedTitle.append(string.uppercased())
            } else {
                updatedTitle.append(string.lowercased())
            }
        }
        return updatedTitle
    }
    
    var json: String {
        return self+".json"
    }
    
    var colorValue: UIColor {
        switch self {
        case "Red":
            return .red
        case "Green":
            return .green
        case "Blue":
            return .blue
        case "Yellow":
            return .yellow
        case "Purple":
            return .purple
        default:
            return .black
        }
    }
}
