//
//  TabBarViewController.swift
//  Recruitment-iOS
//
//  Created by Blazej Krupa on 8/11/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController, NetworkingManagerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        NetworkingManager.sharedManager.delegate = self
        NetworkingManager.sharedManager.downloadItems(type: ItemDataModel.self, url: EndPoint.base.rawValue + "s".json)
    }
    
    deinit {
        print("Deinit", self)
    }

    
    func downloadedItems(result: Result<Any, NetworkError>) {
        switch result {
        case .success(let object):
            if let obj = object as? ItemDataModel {
                obj.data.forEach { SessionManager.Instance.items.insert($0) }
                refreshDataInViewControllers()
            }
        case .failure(let error):
            print(error.localizedDescription)
        }
    }
    
    func refreshDataInViewControllers() {
        guard let vcs = viewControllers else { return }
        for vc in vcs where vc is Loadable {
            (vc as! Loadable).loadData()
        }
    }
}
