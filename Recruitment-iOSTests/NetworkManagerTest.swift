//
//  NetworkManagerTest.swift
//  Recruitment-iOSTests
//
//  Created by Blazej Krupa on 8/12/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class NetworkManagerTest: XCTestCase {
    
    var promise: XCTestExpectation!
    var downloadedItemDetailModel: ItemDetailDataModel!

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        downloadedItemDetailModel = nil
        promise = nil
        NetworkingManager.sharedManager.delegate = nil
        super.tearDown()
    }

    func testValidSlowConnectionForItems() {
        promise = expectation(description: "Valid Items")
        NetworkingManager.sharedManager.delegate = self
        NetworkingManager.sharedManager.downloadItems(type: ItemDataModel.self, url: EndPoint.base.rawValue + "s".json)
        wait(for: [promise], timeout: 2.1)
        XCTAssertEqual(SessionManager.Instance.items.count, 5)
    }
    
    func testValidSlowConnectionForMultipleDownloands() {
        promise = expectation(description: "Valid Items")
        promise.expectedFulfillmentCount = 3
        NetworkingManager.sharedManager.delegate = self
        NetworkingManager.sharedManager.downloadItems(type: ItemDataModel.self, url: EndPoint.base.rawValue + "s".json)
        NetworkingManager.sharedManager.downloadItems(type: ItemDataModel.self, url: EndPoint.base.rawValue + "s".json)
        NetworkingManager.sharedManager.downloadItems(type: ItemDataModel.self, url: EndPoint.base.rawValue + "s".json)
        wait(for: [promise], timeout: 6)
        // it should be always 5 items
        XCTAssertEqual(SessionManager.Instance.items.count, 5)
    }
    
    func testForDetailItemModel() {
        let id = "1"
        promise = expectation(description: "Valid Items \(id)")
        promise.expectedFulfillmentCount = 1
        NetworkingManager.sharedManager.delegate = self
        NetworkingManager.sharedManager.downloadItems(type: ItemDetailDataModel.self, url: EndPoint.base.rawValue + id.json)
        wait(for: [promise], timeout: 2.1)
        XCTAssertNotNil(downloadedItemDetailModel)
        XCTAssertEqual(downloadedItemDetailModel.data.id, id)
    }
    
}

extension NetworkManagerTest: NetworkingManagerDelegate {
    func downloadedItems(result: Result<Any, NetworkError>) {
        switch result {
        case .success(let object):
            switch object {
            case is ItemDataModel:
                for item in (object as! ItemDataModel).data {
                    SessionManager.Instance.items.insert(item)
                }
                promise.fulfill()
            case is ItemDetailDataModel:
                let item = object as! ItemDetailDataModel
                self.downloadedItemDetailModel = item
                promise.fulfill()
            default:
                break
            }
        case .failure(let error):
            print("error:",error.localizedDescription)
        }
    }
}
