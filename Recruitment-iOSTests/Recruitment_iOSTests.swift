//
//  Recruitment_iOSTests.swift
//  Recruitment-iOSTests
//
//  Created by Blazej Krupa on 8/12/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class Recruitment_iOSTests: XCTestCase {
    let model = ItemModel(id: "1", type: "Items", attr: AttributesItemModel(name: "Item1", color: "Red", preview: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."))
    let model2 = ItemModel(id: "1", type: "Items", attr: AttributesItemModel(name: "Item1", color: "Rasasdaed", preview: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."))
    
    func testTitle() {
        let itemViewModel = ItemViewModel(model: model)
        XCTAssertEqual(itemViewModel.name.customTitle(with: 2), "ItEm1")
    }
    
    func testRedableColor() {
        
        let itemViewModel = ItemViewModel(model: model)
        XCTAssertEqual(itemViewModel.color, .red)
    }
    
    func testNotRedableColor() {
        let itemViewModel = ItemViewModel(model: model2)
        XCTAssertEqual(itemViewModel.color, .black)
    }
    
    func testCellSize() {
        let collectionVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CollectionViewControllerId") as! CollectionViewController
        let width = collectionVC.view.layoutMarginsGuide.layoutFrame.width / 2
        let size = collectionVC.collectionView(collectionVC.collectionView, layout: UICollectionViewLayout(), sizeForItemAt: IndexPath(item: 0, section: 0))
        XCTAssertEqual(size, CGSize(width: width, height: width))
    }
    
    func testDealocating() {
        let _ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TableViewControllerId")
        let _ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailsViewControllerId")
        let _ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarViewControllerId")
    }
}

